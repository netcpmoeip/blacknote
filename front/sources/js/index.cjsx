$ = require 'jquery'


$ ->

  $( '.List' ).on 'click', '.surname', ( event )->

    event.preventDefault()

    $item = $( this ).closest '.item'

    opened = ! $item.is '.-open'

    $item.toggleClass '-open', opened

    $item.toggleClass '-closed', ! opened

  ##

  $( '.Header .about' ).on 'click', ( event )->

    event.preventDefault()

    $( '.About.layer' ).removeClass '-closed'

  ##

  $( '.About.layer .close, .About.layer' ).on 'click', ( event )->

    return unless event.target == this

    event.preventDefault()

    $( '.About.layer' ).addClass '-closed'

  ##

  if isDonateWidgetResultPage()

    $( '.Footer .donate' ).hide()

    popupDonateWidget document.body

  else

    $( '.Footer .donate' ).on 'click', ( event )->

      event.preventDefault()

      popupDonateWidget document.body, additionalParams: { shopSuccessURL: window.location.href, shopFailURL: window.location.href }

    ##

  ##

##
