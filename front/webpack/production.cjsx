webpack = require 'webpack'

_ = require 'lodash'

path = require 'path'

ExtractTextPlugin = require 'extract-text-webpack-plugin'


module.exports = ( CONFIG, LOADERS )->

  CONFIG.output.path = path.resolve './builds'

  CONFIG.output.filename = '[name]-[chunkhash].js'

  CONFIG.output.publicPath = '/'


  CONFIG.plugins.push new ExtractTextPlugin '[name]-[contenthash].css'

  CONFIG.plugins.push new webpack.DefinePlugin 'process.env.NODE_ENV': '"production"'

  CONFIG.plugins.push new webpack.optimize.UglifyJsPlugin compress: { warnings: false }


  LOADERS.css.loaders.push 'autoprefixer'

  LOADERS.styl.loaders.splice 1, 0, 'autoprefixer'


  _.each [ 'css', 'styl' ], ( key )->

    loader = LOADERS[ key ]

    loader.loader = ExtractTextPlugin.extract loader.loaders

    delete loader.loaders
