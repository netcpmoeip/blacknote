webpack = require 'webpack'

_ = require 'lodash'

path = require 'path'

HtmlWebpackPlugin = require 'html-webpack-plugin'


CONFIG = {} # https://webpack.github.io/docs/configuration


CONFIG.entry = { blacklist: [ './sources/index' ] }

CONFIG.output = {}


CONFIG.plugins = []

CONFIG.plugins.push new HtmlWebpackPlugin(

  template: 'sources/html/index.html'
  inject: 'head'

)


CONFIG.resolve = {}

CONFIG.resolve.root = path.resolve './sources/js'

CONFIG.resolve.extensions = [

  ''
  '.js'
  '.cjsx'
  '.css'
  '.styl'

]


LOADERS = {

  cjsx: { test: /\.cjsx$/, loaders: [ 'coffee-react-void' ] }

  css: { test: /\.css$/, loaders: [ 'css' ] }

  styl: { test: /\.styl$/, loaders: [ 'css', 'stylus' ] }

  img: { test: /\.(png|jpg|svg)$/, loaders: [ 'url?limit=1000' ] }

}


CONFIG.module = {}

CONFIG.module.loaders = _.values LOADERS


require( './' + ( if process.env.NODE_ENV == 'production' then 'production' else 'development' ) )( CONFIG, LOADERS );


module.exports = CONFIG
