webpack = require 'webpack'

_ = require 'lodash'

path = require 'path'


module.exports = ( CONFIG, LOADERS )->

  _.each CONFIG.entry, ( entry )->

    entry.unshift 'webpack/hot/only-dev-server'

    entry.unshift 'webpack-dev-server/client?http://localhost:8888'


  CONFIG.output.path = path.resolve './public'

  CONFIG.output.filename = '[name].js'

  CONFIG.output.publicPath = 'http://localhost:8888/'


  CONFIG.plugins.unshift new webpack.NoErrorsPlugin()

  CONFIG.plugins.unshift new webpack.HotModuleReplacementPlugin()


  LOADERS.css.loaders.unshift 'style'

  LOADERS.styl.loaders.unshift 'style'


  CONFIG.debug = true

  CONFIG.devtool = 'eval'

  # https://webpack.github.io/docs/webpack-dev-server.html

  CONFIG.devServer = {

    contentBase: CONFIG.output.path
    noInfo: true
    hot: true
    inline: true
    colors: true
    port: 8888

  }
