# coding: utf-8
#
from .settings import *

try:
    from .settings_local import *
except ImportError:
    pass


# Установим локаль по умолчанию.
# reload() нужен потому что разработчики питона умные
# и удаляют функцию setdefaultencoding при загрузке
import sys
reload(sys)
getattr(sys, 'setdefaultencoding')('UTF-8')

