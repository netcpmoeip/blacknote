from django.contrib import admin
from .models import Person
from .models import Event
from .models import File


class EventInline(admin.StackedInline):
    model = Event


class BlacknoteAdmin(admin.ModelAdmin):
    list_display = ('person', 'event')


class PersonAdmin(admin.ModelAdmin):
    inlines = [
        EventInline,
    ]


admin.site.register(Person, PersonAdmin)
admin.site.register(Event, BlacknoteAdmin)
admin.site.register(File)