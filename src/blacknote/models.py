# coding: utf-8
from django.db import models
from taggit.managers import TaggableManager
from sorl.thumbnail import ImageField, get_thumbnail


class Person(models.Model):
    surname = models.CharField('Фамилия', max_length=1000)
    name = models.CharField('Имя', max_length=1000)
    last_name = models.CharField('Отчество', max_length=1000, blank=True)
    position = models.CharField('Должность', max_length=1000)
    pics = ImageField('Фотография', upload_to='img/persons/', null=True)

    def image_tag(self):
        if self.pics:
            return u'<img src="%s%s" />' % (settings.MEDIA_URL,
                                            get_thumbnail(self.pics, '90x90'))
        else:
            return 'без картинки'
    image_tag.short_description = 'картинка'
    image_tag.allow_tags = True

    def __unicode__(self):
        return '%s %s %s' % (self.surname, self.name, self.last_name)

    class Meta:
        unique_together = ('name', 'surname', 'last_name')


class File(models.Model):
    files = models.FileField('Файл', upload_to='img/files/', null=True)


class Event(models.Model):
    person = models.ForeignKey(Person, verbose_name='Кто')
    event = models.TextField('Что сделал')
    details = models.TextField('Подробности', blank=True)
    link = models.URLField('Ссылки на источник', max_length=1000)
    witnesses = models.CharField('Свидетель', max_length=1000)
    data = models.DateField('Дата')
    tags = TaggableManager(blank=True)
    file = models.ForeignKey(File, verbose_name='Файл', default='', null=True)
