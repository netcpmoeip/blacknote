# coding: utf-8
from datetime import datetime
from django.core.management.base import BaseCommand
from blacknote.models import Person, Event


class Command(BaseCommand):
    help = "Import tcv to SQLite"

    def handle(self, *args, **options):
        f = file('db.txt', 'r')

        Person.objects.all().delete()

        for line in f.readlines():

            if not line.strip():
                continue

            a = line.split('\t')
            fio = a[0]
            fio = str(fio).split(' ')

            person, created = Person.objects.get_or_create(surname=fio[0], name=fio[1], last_name=fio[2])
            person.position = a[1]
            person.save()

            print person, created
            print

            # а тут создание эвента
            event = Event(person=person, event=a[2], witnesses=a[3], link=a[4], details=a[5], data=datetime.now())
            event.save()

        f.close()
