# coding: utf-8
from django.shortcuts import render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from models import Person
from django.views.generic import ListView


class PersonView(ListView):
    template_name = 'index.html'
    model = Person


def listing(request):
    persons_list = Person.objects.all()
    paginator = Paginator(persons_list, 3)

    page = request.GET.get('page')
    try:
        person = paginator.page(page)
    except PageNotAnInteger:
        person = paginator.page(1)
    except EmptyPage:
        person = paginator.page(paginator.num_pages)

    return render_to_response('index.html', {"person": person})