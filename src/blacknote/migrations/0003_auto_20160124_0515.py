# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blacknote', '0002_auto_20160124_0507'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='file',
            field=models.ForeignKey(default=b'', verbose_name=b'\xd0\xa4\xd0\xb0\xd0\xb9\xd0\xbb', to='blacknote.File', null=True),
        ),
    ]
