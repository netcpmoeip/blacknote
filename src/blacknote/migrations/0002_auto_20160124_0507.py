# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import sorl.thumbnail.fields
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('blacknote', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('files', models.FileField(upload_to=b'img/files/', null=True, verbose_name=b'\xd0\xa4\xd0\xb0\xd0\xb9\xd0\xbb')),
            ],
        ),
        migrations.AddField(
            model_name='event',
            name='tags',
            field=taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text='A comma-separated list of tags.', verbose_name='Tags'),
        ),
        migrations.AlterField(
            model_name='event',
            name='details',
            field=models.TextField(verbose_name=b'\xd0\x9f\xd0\xbe\xd0\xb4\xd1\x80\xd0\xbe\xd0\xb1\xd0\xbd\xd0\xbe\xd1\x81\xd1\x82\xd0\xb8', blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='link',
            field=models.URLField(max_length=1000, verbose_name=b'\xd0\xa1\xd1\x81\xd1\x8b\xd0\xbb\xd0\xba\xd0\xb8 \xd0\xbd\xd0\xb0 \xd0\xb8\xd1\x81\xd1\x82\xd0\xbe\xd1\x87\xd0\xbd\xd0\xb8\xd0\xba'),
        ),
        migrations.AlterField(
            model_name='event',
            name='person',
            field=models.ForeignKey(verbose_name=b'\xd0\x9a\xd1\x82\xd0\xbe', to='blacknote.Person'),
        ),
        migrations.AlterField(
            model_name='person',
            name='last_name',
            field=models.CharField(max_length=1000, verbose_name=b'\xd0\x9e\xd1\x82\xd1\x87\xd0\xb5\xd1\x81\xd1\x82\xd0\xb2\xd0\xbe', blank=True),
        ),
        migrations.AlterField(
            model_name='person',
            name='pics',
            field=sorl.thumbnail.fields.ImageField(upload_to=b'img/persons/', null=True, verbose_name=b'\xd0\xa4\xd0\xbe\xd1\x82\xd0\xbe\xd0\xb3\xd1\x80\xd0\xb0\xd1\x84\xd0\xb8\xd1\x8f'),
        ),
        migrations.AlterUniqueTogether(
            name='person',
            unique_together=set([('name', 'surname', 'last_name')]),
        ),
        migrations.AddField(
            model_name='event',
            name='file',
            field=models.ForeignKey(default=b'', verbose_name=b'\xd0\xa4\xd0\xb0\xd0\xb9\xd0\xbb', to='blacknote.File'),
        ),
    ]
